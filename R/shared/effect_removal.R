# Author: Michael Finlayson
#
# Description:
# This object will create a list of umi_counts and meta_data for each library 
# that can be found within or below the current directory or directory 
# provided
#


######## -----------------------------------------------------------------------
########                 Definition                                     
######## ---------------------------------------------------------------

library(Matrix)

EffectRemoval = setClass('EffectRemoval', 
  slots=c(
    model_matrix = 'matrix',
    lm_coefficients = 'matrix',
    adjusted_norm_exp_val = 'matrix'
  )
)


######## -----------------------------------------------------------------------
########                 Initialization                                 
######## ---------------------------------------------------------------

setMethod('initialize', 'EffectRemoval', function(.Object, norm_ev, meta_data,
                                                  vars_to_regress, vars_to_preserve) { 
  
  # validate input
  stopifnot(all(colnames(norm_ev)==rownames(meta_data)))
  stopifnot(class(vars_to_regress)=='character')
  stopifnot(all(vars_to_regress %in% colnames(meta_data)))
  stopifnot(all(vars_to_preserve == 'none') ||
              (all(vars_to_preserve %in% colnames(meta_data)) &&
                 length(intersect(vars_to_regress, vars_to_preserve)) == 0))
  
  # if vars_to_preserve is none, make it work with the formula string creation code
  if (all(vars_to_preserve == 'none')) { vars_to_preserve = c() }
  
  # create the model Matrix
  mm_formula = formula(paste('~', paste(c(vars_to_regress, vars_to_preserve), collapse = ' + ')))
  model_matrix = model.matrix(mm_formula, data = meta_data)
  
  # create the QR decomposition
  mm_QR = qr(model_matrix)
  
  # now fit each gene with the QR decomposition
  lm_coef = t(sapply(rownames(norm_ev), function(gene_name, mm_QR, norm_ev) {
    
    gnev = norm_ev[gene_name,]
    
    gene_coefficients = qr.coef(mm_QR, gnev)
    
    return(gene_coefficients)
  }, mm_QR, norm_ev))
  
  # ignore NA values
  lm_coef[is.na(lm_coef)] = 0
  
  # now replace each column in norm_ev
  for (i in 1:ncol(norm_ev)) {
    
    cell_effects = c()
    for (vtr in vars_to_regress) { cell_effects = c(cell_effects, paste(vtr,meta_data[i,vtr],sep='')) }
    
    cell_effects = cell_effects[cell_effects %in% colnames(lm_coef)]
    
    if(length(cell_effects)==0) { next }  # if there is nothing to remove
    
    norm_ev[,i] = norm_ev[,i] - apply(lm_coef[,cell_effects,drop=F], 1, sum)
  }
  
  # compile object
  .Object@model_matrix = model_matrix
  .Object@lm_coefficients = lm_coef
  .Object@adjusted_norm_exp_val = norm_ev
  
  return(.Object) 
})

######## -----------------------------------------------------------------------
########                 Methods                                        
######## ---------------------------------------------------------------

###### ------------------------------
######     <name>

# setGeneric('<name>', def=function() {standardGeneric('<name>')})
# 
# setMethod('<name>', signature(), 
# function(sc_data, cells_to_keep) {})


